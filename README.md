#manu-data-challenge

This a challenge resolution for the last fm dataset proposed. 

##Services
* Level1 - This service is responsible to produce a list of Users and the number of distinct songs the user has played.
* Level2 - Service that produces a list of 100 most popular Song titles, its Artist, and the number of times the songs have been played.
* Level3 - a list of 10 longest sessions by elapsed time with the following details for each session. The list contains
user, Time of the first Song played, Time of the last Song played, duration of the session in minutes, number of songs played,
and a list of songs played.

##Test
A battery of tests have been built in order to resolve the challenge according with TDD practices and to ensure obtaining
the expected request.

##Results
The resolution of the challenge consist in 3 csv files containing the relevant results per level. The files can be found
under `output/level1`, `output/level2` and `output/level3`. 
An example of the result can be visualised [here](https://docs.google.com/spreadsheets/d/1y9AqJfoLg8GE2gZe3OcSj1yQIRlvZE9l_i3z158Lg9k/edit?usp=sharing)

The resolution times observed when running a spark cluster in a local machine are the following

* Level1 - 81 seconds
* Level2 - 72 seconds
* Level3 - 120 seconds

##Example
Run tests and build program `mvn clean compile test assembly:single` 

Execute the program `java -cp  target/test-lastfm-1.0-SNAPSHOT-jar-with-dependencies.jar com.manumaqueda.data.lastfm.Main <input-file-path>`

##Assumptions:
* Each of the rows represents a single song execution. For instance we can't have two rows with the same song name, artist
 user and timestamp.
* File is ordered by user id and then by timestamp descending.
* Just the file with name `userid-timestamp-artid-artname-traid-traname.tsv` is needed for the resolution of the program and it should be used as the only input param  
