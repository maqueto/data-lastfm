package com.manumaqueda.data.lastfm.model;


import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Session implements Serializable {
    public final static int MAX_SESSION_THRESHOLD_IN_MINUTES = 20;
    public final static int MAX_SESSION_THRESHOLD_IN_SECONDS = 20 * 60;
    private String userId;
    private Instant startTime;
    private Instant endTime;
    private List<SongExecution> songExecutions;

    public Session(SongExecution songExecution){
        this.userId = songExecution.getUserId();
        this.startTime = songExecution.getTimestamp();
        this.endTime = songExecution.getTimestamp();
        this.songExecutions = new ArrayList<>();
        this.songExecutions.add(songExecution);
    }
    public void includeSong(SongExecution songExecution){
        this.startTime = songExecution.getTimestamp();
        this.songExecutions.add(songExecution);
    }
    // we expect to executed some time before the
    public boolean sameSession(SongExecution songExecution){
        if(this.userId.equals(songExecution.getUserId()) &&
                Math.abs(ChronoUnit.MINUTES.between(this.startTime,songExecution.getTimestamp())) <= MAX_SESSION_THRESHOLD_IN_MINUTES)
            return true;
        return false;
    }
    public Long getElapsedDurationSeconds(){
        return Math.abs(ChronoUnit.SECONDS.between(startTime,endTime));
    }
    public Long getElapsedDurationMinutes(){
        return ChronoUnit.MINUTES.between(startTime,endTime);
    }

    public List<SongExecution> getSongExecutions() {
        return songExecutions;
    }

    public String getUserId() {
        return userId;
    }
}
