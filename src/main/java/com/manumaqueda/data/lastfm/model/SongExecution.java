package com.manumaqueda.data.lastfm.model;

import java.io.Serializable;
import java.time.Instant;

public class SongExecution implements Serializable {
    private String userId;
    private Instant timestamp;
    private Song song;

    public SongExecution(String userId, Instant timestamp,Song song) {
        this.userId = userId;
        this.timestamp = timestamp;
        this.song = song;
    }

    public String getUserId() {
        return userId;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getSongName() {
        return this.song.getName();
    }


}
