package com.manumaqueda.data.lastfm.model;


import java.io.Serializable;
import java.util.Objects;

public class Song implements Serializable {
    private String artistId;
    private String artistName;
    private String name;

    public Song(String artistId, String artistName, String name) {
        this.artistId = artistId;
        this.artistName = artistName;
        this.name = name;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Song)) {
            return false;
        }
        Song song = (Song)obj;
        return this.name.equals(song.getName()) && this.artistName.equals(song.artistName)
                && this.artistId.equals(song.artistId);
    }

    @Override
    public String toString() {
        return "Song{" +
                "artistId='" + artistId + '\'' +
                ", artistName='" + artistName + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
    @Override
    public int hashCode() {
        return Objects.hash(name,artistId,artistName);
    }
}
