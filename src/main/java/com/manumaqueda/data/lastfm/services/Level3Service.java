package com.manumaqueda.data.lastfm.services;


import com.manumaqueda.data.lastfm.model.Session;
import com.manumaqueda.data.lastfm.model.Song;
import com.manumaqueda.data.lastfm.model.SongExecution;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Serializable;
import scala.Tuple2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.Instant;
import java.util.*;

public class Level3Service {
    private static JavaSparkContext sc;
    final static CompareSessionByElapsedTime compareSessions = new CompareSessionByElapsedTime();

    public Level3Service(JavaSparkContext sc) {
        this.sc = sc;
    }
    static class CompareSessionByElapsedTime implements Comparator<Session>, Serializable {
        @Override
        public int compare(Session s1, Session s2) {
            return -s1.getElapsedDurationSeconds().compareTo(s2.getElapsedDurationSeconds());
        }
    }

    public JavaPairRDD<String, Iterable<Session>> sessionsByUser(String songsListPath) {
        JavaRDD<String> songListInputFile = sc.textFile(songsListPath);
        // user and song executions
        JavaPairRDD<String, SongExecution> songsExecutionsbyUser = songListInputFile.mapToPair(s -> {
            String[] executionSplit = s.split("\t");
            Song song = new Song(executionSplit[4],executionSplit[3],executionSplit[5]);
            return new Tuple2<String, SongExecution>(executionSplit[0],
                    new SongExecution(executionSplit[0], Instant.parse(executionSplit[1]),song));
        });
        // group song executions by user
        JavaPairRDD<String, Iterable<SongExecution>> songsExecutionPerUser = songsExecutionsbyUser.groupByKey();

        // build sessions
        JavaPairRDD<String, Iterable<Session>> sessionsPerUser = songsExecutionPerUser.mapToPair( tuple2 ->{
            List<Session> sessions = new ArrayList<>();
            Iterator<SongExecution> userSongIterable = tuple2._2().iterator();
            Session session = new Session(userSongIterable.next());
            sessions.add(session);
            while(userSongIterable.hasNext()){
                SongExecution nextSongExecution = userSongIterable.next();
                if(session.sameSession(nextSongExecution))
                    session.includeSong(nextSongExecution);
                else{
                    session = new Session(nextSongExecution);
                    sessions.add(session);
                }
            }
            return new Tuple2<>(tuple2._1(),sessions);
        });
        return sessionsPerUser;
    }

    /**
     * 10 longest sessions by elapsed time with the following details for each session
     * @param songsListPath
     * @return
     */
    public List<Session> run(String songsListPath) {
        List<Session> longest10Sessions = new ArrayList<>();
        JavaPairRDD<String, Iterable<Session>> sessionsPerUser = sessionsByUser(songsListPath);
        JavaRDD<Session> allSessions =  sessionsPerUser
                .flatMap(sessions -> sessions._2().iterator());
        allSessions.takeOrdered(10, compareSessions)
                .forEach( t -> longest10Sessions.add(t));
        return longest10Sessions;
    }
    public boolean execute(String songsListPath, String outputFolderPath){
        List<Session> largest10Sessions = run(songsListPath);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFolderPath+"/level3.csv"));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("User Id","Time of First Song","Time of Last Song","Elapsed Time (mins)", "#Song","Songs"))
        ) {
            for(Session session: largest10Sessions){
                SongExecution firstSong = session.getSongExecutions().get(0);
                SongExecution lastSong = session.getSongExecutions().get(session.getSongExecutions().size() - 1);
                StringBuilder songsStr = new StringBuilder("[");
                session.getSongExecutions().stream().forEach(song -> songsStr.append(song.getSongName()).append(","));
                songsStr.deleteCharAt(songsStr.length()-1);
                songsStr.append("]");
                csvPrinter.printRecord(session.getUserId(),firstSong.getTimestamp(),lastSong.getTimestamp(),
                        String.valueOf(session.getElapsedDurationMinutes()),session.getSongExecutions().size(),songsStr.toString());
            }
            csvPrinter.flush();
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
