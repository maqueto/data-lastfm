package com.manumaqueda.data.lastfm.services;
import com.manumaqueda.data.lastfm.model.Song;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Serializable;
import scala.Tuple2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Produce a list of Users and the number of distinct songs the user has played.
 */
public class Level1Service {
    public static JavaSparkContext sc;

    public Level1Service(JavaSparkContext sc) {
        this.sc = sc;
    }
    static class CompareUserSongs implements Comparator<Tuple2<String, Integer>>, Serializable {
        @Override
        public int compare(Tuple2<String, Integer> tuple1, Tuple2<String, Integer> tuple2) {
            return -tuple1._2().compareTo(tuple2._2());
        }
    }

    public boolean execute(String songsListPath, String outputFolderPath){
        List<Tuple2<String,Integer>> numSongsPerUser = run(songsListPath);
        // create temp csv
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFolderPath+"/level1.csv"));
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withHeader("User Id", "# Songs"))) {
            for(Tuple2 t: numSongsPerUser){
                csvPrinter.printRecord(t._1(), t._2());
            }
            csvPrinter.flush();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public List<Tuple2<String, Integer>> run(String songsListPath) {
        JavaRDD<String> songListInputFile = sc.textFile(songsListPath);
        JavaPairRDD<String, String> userSongExecution = songListInputFile.mapToPair(s -> {
            String[] executionSplit = s.split("\t");
            return new Tuple2<String, String>(executionSplit[0], executionSplit[5]);
        });
        List<Tuple2<String, Integer>> result = userSongExecution.distinct()
                .mapToPair(tuple -> new Tuple2<String, Integer>(tuple._1, 1))
                .reduceByKey((accum, n) -> (accum + n)).collect();
        CompareUserSongs compare = new CompareUserSongs();
        return result.stream().sorted(compare).collect(Collectors.toList());
    }
}
