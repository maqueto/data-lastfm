package com.manumaqueda.data.lastfm.services;

import com.manumaqueda.data.lastfm.model.Song;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import scala.Serializable;
import java.time.Instant;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Level2Service {
    private static JavaSparkContext sc;

    public Level2Service(JavaSparkContext sc) {
        this.sc = sc;
    }
    static class CompareSongsByPlayedTimes implements Comparator<Tuple2<Song, Integer>>, Serializable {
        @Override
        public int compare(Tuple2<Song, Integer> tuple1, Tuple2<Song, Integer> tuple2) {
            return tuple2._2 - tuple1._2;
        }
    }
    public boolean execute(String songsListPath, String outputFolderPath){
        Instant startTime = Instant.now();
        List<Tuple2<Song,Integer>> top100Songs = run(songsListPath);
        // create temp csv
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFolderPath+"/level2.csv"));
             CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                     .withHeader("Song Name","Artist Name","Artist Id", "# Songs"))
        ) {
            for(Tuple2 tuple: top100Songs){
                Song s = (Song)tuple._1();
                csvPrinter.printRecord(s.getName(),s.getArtistName(),s.getArtistId(),tuple._2());
            }
            csvPrinter.flush();
        }catch (Exception e){
            return false;
        }
        return true;
    }
    public List<Tuple2<Song,Integer>> run(String songsListPath) {
        Map<Integer,Song> result = new HashMap<>();
        JavaRDD<String> songListInputFile = sc.textFile(songsListPath);
        JavaPairRDD<Song, Integer> songsExecutions = songListInputFile.mapToPair(s -> {
            String[] executionSplit = s.split("\t");
            Song song = new Song(executionSplit[4],executionSplit[3],executionSplit[5]);
            return new Tuple2<Song, Integer>(song,1);
        });
        // song is equals if artist name and artist id is the same
        JavaPairRDD<Song,Integer> executionsPerSong = songsExecutions.reduceByKey((accum, n) -> (accum + n));
        // get top 100
        CompareSongsByPlayedTimes compareSongs = new CompareSongsByPlayedTimes();
        List<Tuple2<Song,Integer>> top100Songs = executionsPerSong
                .takeOrdered(100,compareSongs);
        return top100Songs;
    }
}
