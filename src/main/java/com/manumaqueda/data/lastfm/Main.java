package com.manumaqueda.data.lastfm;

import com.manumaqueda.data.lastfm.services.Level1Service;
import com.manumaqueda.data.lastfm.services.Level2Service;
import com.manumaqueda.data.lastfm.services.Level3Service;
import org.apache.spark.api.java.JavaSparkContext;

import java.time.Duration;
import java.time.Instant;


public class Main {

    public static void main(String... args){
        System.out.println("###### Starting lastfm data analysis #######");
        if (args.length != 1) {
            System.out.println("Please provide input file path");
            return;
        }
        Instant start = Instant.now();
        JavaSparkContext sc = new JavaSparkContext("local", "lastfm-data");
        sc.setLogLevel("ERROR");
        try{
            String inputPath = args[0];
            Level1Service level1 = new Level1Service(sc);
            Level2Service level2 = new Level2Service(sc);
            Level3Service level3 = new Level3Service(sc);
            System.out.println("[Level 1] Starting...");
            Instant auxStart = Instant.now();
            level1.execute(inputPath,"output/level1/");
            System.out.println("[Level 1] Finished in "+Duration.between(auxStart,Instant.now()).getSeconds()+" seconds");
            System.out.println("[Level 2] Starting...");
            auxStart = Instant.now();
            level2.execute(inputPath,"output/level2/");
            System.out.println("[Level 2] Finished in "+Duration.between(auxStart,Instant.now()).getSeconds()+" seconds");
            System.out.println("[Level 3] Starting...");
            auxStart = Instant.now();
            level3.execute(inputPath,"output/level3/");
            System.out.println("[Level 3] Finished in "+Duration.between(auxStart,Instant.now()).getSeconds()+" seconds");
            sc.stop();
        }catch (Exception e){
            System.out.println("Something wrong happened, please make sure program is invoked as specified in README.md or " +
                    "contact the maintainer");
            sc.stop();
        }
        System.out.println("Execution time -> "+Duration.between(start,Instant.now()).getSeconds()/60 +" minutes.");
        System.out.println("###### Finalising lastfm data analysis #######");


    }
}
