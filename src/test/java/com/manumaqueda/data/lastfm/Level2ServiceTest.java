package com.manumaqueda.data.lastfm;

import com.manumaqueda.data.lastfm.model.Song;
import com.manumaqueda.data.lastfm.services.Level2Service;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import scala.Tuple2;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import static com.manumaqueda.data.lastfm.Level1ServiceTest.LARGE_INPUT_FILE;

public class Level2ServiceTest {
    private transient JavaSparkContext sc;

    @Before
    public void setUp() {
        sc = new JavaSparkContext("local", "Level2Test");
    }

    @After
    public void tearDown() {
        if (sc != null){
            sc.stop();
        }
    }

    @Test
    public void testSampleFile() {
        Level2Service level2 = new Level2Service(sc);
        List<Tuple2<Song,Integer>> result = level2.run("test-input/Songs-Level2.tsv");
        Assert.assertEquals(100, result.size());
        Assert.assertEquals("Happyend (Live_2009_4_15)", result.get(0)._1().getName());
    }
    public void testOriginalFile() {
        Instant start = Instant.now();
        Level2Service level2 = new Level2Service(sc);
        List<Tuple2<Song,Integer>> result = level2.run(LARGE_INPUT_FILE);
        System.out.println("Duration of execution -> "+Duration.between(start,Instant.now()).getSeconds()+" seconds.");
        Assert.assertEquals(100, result.size());
        Assert.assertEquals("Such Great Heights", result.get(0)._1().getName());
        Assert.assertEquals(new Integer(3663), result.get(1)._2());
    }

}
