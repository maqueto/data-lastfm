package com.manumaqueda.data.lastfm;

import com.manumaqueda.data.lastfm.model.Session;
import com.manumaqueda.data.lastfm.model.Song;
import com.manumaqueda.data.lastfm.model.SongExecution;
import com.manumaqueda.data.lastfm.services.Level2Service;
import com.manumaqueda.data.lastfm.services.Level3Service;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.spark_project.guava.collect.Iterators;
import scala.Tuple2;

import java.nio.channels.SeekableByteChannel;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static com.manumaqueda.data.lastfm.Level1ServiceTest.LARGE_INPUT_FILE;

public class Level3ServiceTest {
    private transient JavaSparkContext sc;

    @Before
    public void setUp() {
        sc = new JavaSparkContext("local", "Level3Test");
    }

    @After
    public void tearDown() {
        if (sc != null){
            sc.stop();
        }
    }

    @Test
    public void userSessionsSampleFile_1() {
        //when taking all the songs
        Level3Service level3 = new Level3Service(sc);
        // filtering by session
        Map<String,Iterable<Session>> result = level3.sessionsByUser("test-input/Songs-Level3-a.tsv").collectAsMap();
        Assert.assertEquals(5, result.size());
        // for user user_000001 we have 52 sessions
        Assert.assertEquals(49, Iterators.size(result.get("user_000001").iterator()));
        // for user user_000001, last session has 6 songs
        Session lastSessionUser1 = result.get("user_000001").iterator().next();
        Assert.assertEquals(5, lastSessionUser1.getSongExecutions().size());
        // for user user_000001, last song of last session is Backspin
        Assert.assertEquals("Backspin",
                lastSessionUser1.getSongExecutions().get(0).getSongName());
    }
    @Test
    public void userSessionsSampleFile_2() {
        //when taking all the songs
        Level3Service level3 = new Level3Service(sc);
        // filtering by session
        Map<String,Iterable<Session>> result = level3.sessionsByUser("test-input/Songs-Level3-b.tsv").collectAsMap();
        Assert.assertEquals("number of users in the file",8, result.size());
        // for user user_000692 we have 2 sessions
        Assert.assertEquals(2, Iterators.size(result.get("user_000692").iterator()));
        // for user user_000692, last session of user
        Session lastSessionUser1 = result.get("user_000692").iterator().next();
        Assert.assertEquals(5, lastSessionUser1.getSongExecutions().size());
        // for user user_000692, last song of last session is Death In Fire
        Assert.assertEquals("Death In Fire",
                lastSessionUser1.getSongExecutions().get(0).getSongName());
    }
    @Test
    public void userSessionsSampleFile_3() {
        //when taking all the songs
        Level3Service level3 = new Level3Service(sc);
        // filtering by session
        Map<String,Iterable<Session>> result = level3.sessionsByUser("test-input/Songs-Level3-c.tsv").collectAsMap();
        Assert.assertEquals("number of users in the file",1, result.size());
        Assert.assertEquals(3, Iterators.size(result.get("user_000949").iterator()));
        Session lastSessionUser1 = result.get("user_000949").iterator().next();
        Assert.assertEquals(4,lastSessionUser1.getSongExecutions().size());
        SongExecution lastSongUser1 = lastSessionUser1.getSongExecutions().get(0);
        Assert.assertEquals("Happy Face", lastSongUser1.getSongName());
        Assert.assertEquals("2006-02-27T17:57:16Z", lastSongUser1.getTimestamp().toString());
    }
    @Test
    public void longest10Sessions() {
        //when taking all the songs
        Level3Service level3 = new Level3Service(sc);
        // get longest 10 sessions
        List<Session> result = level3.run("test-input/Songs-Level3-b.tsv");
        // 10 sessions
        Assert.assertEquals("10 sessions",10, result.size());
        // longest session - user user_000001
        Session longestSession = result.iterator().next();
        Assert.assertEquals("user longest session","user_000001", longestSession.getUserId());
        // longest session last song
        SongExecution lastSongExecutionLongestSession = longestSession.getSongExecutions().iterator().next();
        Assert.assertEquals("Behind The Mask (Live_2009_4_15)", lastSongExecutionLongestSession.getSongName());
        Assert.assertEquals("2009-04-17T01:34:24Z", lastSongExecutionLongestSession.getTimestamp().toString());
    }
}
