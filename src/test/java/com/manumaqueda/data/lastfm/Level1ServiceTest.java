package com.manumaqueda.data.lastfm;

import com.manumaqueda.data.lastfm.services.Level1Service;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import scala.Tuple2;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;

public class Level1ServiceTest {
    public static final String LARGE_INPUT_FILE = "/Users/manumaqueda/Documents/data/lastfm-dataset-1K/userid-timestamp-artid-artname-traid-traname.tsv";
    private transient JavaSparkContext sc;

    @Before
    public void setUp() {
        sc = new JavaSparkContext("local", "Level1Test");
    }

    @After
    public void tearDown() {
        if (sc != null){
            sc.stop();
        }
    }

    @Test
    public void testSampleFile() {
        Level1Service level1 = new Level1Service(sc);
        List<Tuple2<String,Integer>> result = level1.run( "test-input/Songs-Level1.tsv");
        Assert.assertEquals("user_000001", result.get(0)._1());
        Assert.assertEquals(new Integer(218), result.get(0)._2());
    }
    public void testOriginalFile() {
        Instant start = Instant.now();
        Level1Service level1 = new Level1Service(sc);
        List<Tuple2<String,Integer>> result  = level1.run(LARGE_INPUT_FILE);
        System.out.println("Duration of execution -> "+Duration.between(start,Instant.now()).getSeconds()+" seconds.");
        Assert.assertEquals(992, result.size());
    }
}
